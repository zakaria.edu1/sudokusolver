# Sudoku Solver With SAT

This is the code for my final project, the sudoku solver using a SAT algorithm, in this project i implement the algorithm with Python 3, and with Django as the framework

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need:

* [Python 3.6.5](https://www.python.org/downloads/release/python-365/) - The language used
* [Pycosat 0.6.3](https://pypi.org/project/pycosat/) - The library used
* [Django 1.9.13](https://www.djangoproject.com/download/) - The web framework used

Install that to your virtual environtment

### How to make virtual environtment?

Run:
```
python3 -m venv myvenv
```
that way you make a venv with the name of myenv


Start your virtual environment by running:
```
source myvenv/bin/activate
```

### Installing and running the app locally

Install your Django and Pycosat by running:
```
pip install django==1.9.13
pip install pycosat
```

Run the app locally by running:
```
python manage.py migrate
python manage.py runserver
```

you can use this app locally, if you needed a more detailed instalation guide you can access this [guide.](#)

### More thing to learn
If you need more knowledge about Django you can check [this tutorial.](https://tutorial.djangogirls.org/en/)